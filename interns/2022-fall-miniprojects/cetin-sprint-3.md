# sprint-3-cetin

## Sprint 3

- [ ] R: rochdi will build `localhost_bundle.js`
- [ ] R: fix the broken deployment on qary.ai (vish pushed bugs)
- [ ] test on localhost
- [ ] update yaml file
- [ ] load yaml into database locally
- [ ] load yaml into database on render for Cetin's local account
- [ ] load yaml into database on render for qary.ai on render

# - [ ] update instead of overwrite data in database

### Enhancements

- [ ] additional correct strings for 1+1: two, 2
- [ ] additional accepted strings for start: begin, ready

- [ ] if yaml file fails to load try making state names unique by using "quizbot-" prefix on all state names
