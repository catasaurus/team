# Student Success Predictor

## Introduction

### The Problem
Online learning has been a growing and maturing industry for many years. While Khan Academy is a leader in self driven oneline learning, massive open online courses (MOOCs), like Coursera, create an even more structured approach.  I took my first online course on coding fundamentals from EdX in 2013 and started my data science journey in 2019 on Coursera.  The availability of quality and accessible online learning has had an important impact on my life.
While these are an amazing resources, they also have high dropout and failure rates.  Many students, accustomed to the accountability features built into traditional face-to-face learning, struggle with the openness of these systems, the looser structure and deadlines, and the lack accountability to another person.  In self-driven learning it can be easy to get lost or give up.

### The Solution
I set out to find ways to use data to help students succeed by identifying those who are in danger of failing or withdrawing early.  Many students in need of special intervention to improve their chances of success can be identified by the way they interact with the online learning system itself.
The goal of this project is to model student interactions with an online learning environment to predict whether they will ultimately pass or fail a course early enough for intervention to be effective.  My model uses the patterns of interaction between student and virtual learning environment that can predict the success of students in self-paced online learning environments.

## Extraction, Transformation, and Loading the Data

### Extraction

My data comes from a fully online university in Great Britain called [The Open University](http://www.open.ac.uk/).  This accredited university offers self-paced courses in many subjects.  Courses have beginnings and ends, but the assignments and assessments are completed at the student's pace within that framework.  This is important because the timing of the completion of assessments is a feature I will use in my model.  

The university has released, for scientific research, a dataset of ~30k students divided among 7 different modules and spread over 4 cohorts, spring and autumn, over a 2 year period.  This dataset is available [here](https://analyse.kmi.open.ac.uk/open_dataset) along with meta data helpful to understanding it.  The dataset include some demographic information about the students such as gender and deprecation, assessment scores, and course outcomes.  It also includes trace data of students interacting with the virtual learning environment.  This data is provided on a per activity basis and includes the number of clicks for each activity each student interacted with as part of the program.

I used this trace data as well as data about assessment scores and dates of assessment submissions.

### Transformation

The trace data is dated relative to the start date of the course, and student can start working on activities up to 25 days before the official start of the course.  I used this data to create a wide form time series data table.  Each row is a student enrolled in a course, or a registration.  I don't use students themselves as observations because some students take multiple courses and some student repeat courses.  I also only use data from the first `n` days of the course, since our goal is predict which students might need intervention *before* the end of the course, and early enough for intervention to be effective.  Somewhat arbitrarily I chose the mid point of the courses, but this date is easily tweaked.  It can be extended to create more accurate predictions, or retracted to provide more time for interventions to change student behavior, or prevent more withdrawals.  After all, the further into the course, the more student will have withdrawn.

Each column represents data relevant to that student for a particular day in the course.  For instance, column 1 represents the number of activities a student completed on day -25 (remember, student can start working early), column 2 represents the number of times a student clicked on activities for day -25, and column 3 represents a polynomial feature engineered by multiplying activities by clicks.  After the columns for the final day of the course I have added columns for assessment scores and the timing of assessment completion compared to the course suggested soft deadline.

#### Features:

1. **Count of activities completed on each day of the course.**
2. **Sum of total clicks performed on each day of the course.**  This number is only loosely correlated to the number of activities because there are many kinds of activities.  Some are a URL to an outside resource, and a student won't only be expected to click on it once.  Others are quizzes or interactive activities which we might expect a student to click on many times.
3. **Activity counts times clicks.**  This features helps to correct for the above discrepency in clicks per activity by capturing information about total work done by a student in a day, increasing the value by either clicks or activities.  It also creates a non-linear feature to capture a polynomial relationship between the above features.
4. **Assessment scores for each assessment in order of earliest to latest.**  My previous work with this dataset found assessment scores to be highly correlated with student success.
5. **Timing of assessment submissions for each assessment.** Previous modeling found the number of assessments a student had completed by prediction time was highly correlated to student success as well.  These variables represent how many days before or after the soft deadline for each assessment the student submitted it.  I chose not to include the assessment timings in with the rest of daily records because each course had very different deadlines for these assessments as well as different numbers of assessments.  I feared that peppering them through the daily time series features would confound the model, and that the relative date of submission would help to normalize the differences between courses.
6. **Module name: one-hot encoded.** Different modules in the dataset not only had different work and assessment requirements, they had very different completion rates.  In other words some seemed to be more difficult than others.  The specific model that a student was enrolled in turns out to be very highly correlated to student success.  I had endeavored to make my model generalizable to other datasets from other online learning organizations, but the importance of taking into consideration which course a student is taking is crucial to predicting their success.  Future users of this model can change these features to represent the courses they want to predict, or can remove them if they are training model to make predictions only on a single course.

#### Other Notes on Transformation

1. I dropped observations of student who had withdrawn before the prediction window.  Those student outcomes were already known and do not need to be predicted.

2. The original target data described four classes: 

    a. 'Distinction' (students who did particularly well in a course)
    b. 'Pass'
    c. 'Fail'
    d. 'Withdrawn'
    
I combined pass and distinction into class 0, and fail and withdrawn into class 1.  Students who do not pass the course are the student we want to identify, so they are the positive class.  

### Loading

My data is loaded into a table with 24005 rows (this changes depending on which day of the course the prediction is being made) and 504 columns.  For modeling purposes 20% of the data is held out for final model evaluation and the remaining data is split 80/20 into a training set and a validation set to test each model candidate.  

The data has a 3-1 ratio of student who pass and students who do not.  While this is not an extreme imbalance, I use a synthetic oversampling technique called SMOTE provided by imbalanced learn or imblearn (which can be pip installed and plays fairly well with sklearn functions) to balance the classes.  SMOTE uses K-nearest neighbors to project the data into n-features number of dimensions, finds clusters of data representing each class, pass or fail, and selects points within those clusters not already representing data to create new data points.  The new data points are not real students, but they are *similar* to students in the under-representated class, in this case students in need of intervention.

### EDA


### Model Tuning

#### Creating a Short-list of Models to Tune

I chose Receiver Operating Characteristic Area Under the Curve, or ROC AUC, as my target metric because my model is tunable to different probability thresholds in order to maximize either recall or precision in predicting student success.  By probability threshold I mean at what probability the model recommends intervention.  The classifier models output a probability of an observation belonging to a given class and by default it assigns prediction probabilities greater than 50% to the positive class and less that 50% to the negative class.  By changing this threshold we can essentially tell the model, "If you think there is only a 40% chance that a student needs intervention, then I still want to you recommend that." Or, "I only want you to recommend an intervention for a student if you are 80% sure they need it."

If interventions are expensive, a stakeholder may want to focus on the precision of the model predictions to ensure that they students they go to *really* need them and minimize wasted interventions.  If student failure is more expensive, and/or interventions are cheap, they would want to tune the probability thresholds to maximize the model's recall of students in need of intervention to prevent as many failures and withdrawals as possible, even at the expense of more students who don't need interventions receiving them anyway.

ROC AUC will measure a model's performace across all probability thresholds.

I tried a large variety of out of the box models available in the Sklearn package and found that tree-based models were most successful.  I decided to do further hyperparameter tuning on a decision tree, random forest (an ensemble method), and eXtreme Gradient Boost (a boosting model).  Adaboost also showed up high on the list, but is slow to train and even slower to tune, so I did not include it in further experiments.  Model tuning is time consuming, and I wanted to maximize my chances of finding a good model in a reasonable amount of time.  

#### Optimizing Hyperparameters
I then used the sci-kit learn optimization, or skopt package (installable with conda forge) to perform a Bayesian hyperparameter search to find the right model settings to tune a model of each of the above types.  This search algorithm tests models by selecting hyperparameter values from a use-defined distribution, then using previous iterations to inform each new iterative attempt to create the best tuned model.  It converges more quickly than a complete grid search of all possible combinations of model hyperparameters.

#### Hyperparameter Tuning Experimental Record
I saved each trained model's results in a hyperparameter table for comparison.  Individual model results can be extraced from this table using the script `table[table['model'] == '<model-to-explore>'].dropna(axis=1, how='all')`  Since there are features in the table for all hyperparameters tested for all models, the `.dropna()` at the end of the script will make sure you only see hyperparameter values for the model you want to explore.
