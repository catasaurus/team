# Isaac's Notes

I will be keeping track of my progress throughout the course of the internship here. This report will contain learning notes, documentation of projects etc.

## Notes from Week 1 Meeting with Hobson

1. Determine projects of interest for the duration of the internship. I suggested something to do with Social Media. Hobson suggested to look into Open Source Protocols and Social Platforms like:

    * mastodon.social

    * Tusky

    * ActivityPub

    Hobson also suggested to skim through the list of projects.

2. Setup my checklist for skills to develop during the course of the internship.

## Some Project Ideas

Beginner - A Chatbot

Intermediate - Intent mining for user utterances. Could be combined with Sentiment Analysis?

* learn to use FastText to create embeddings sentences in English
* use FastText to create embeddings for sentences in your own language
* create embeddings for labeled dataset of user utterances provided by Tangible AI.
* Use a K-means or DBScan algorithm to perform clustering of the utterances based on their embedding.
* Create a function that receives a list of utterances as input and returns a list of clusters, where each intent belongs to just one cluster.
  
Advanced -  Book visualizer: Summarize the sections of a book

* asciidoc parser to create python data structure: [asciidoc3](https://gitlab.com/asciidoc3/asciidoc3)
* find a pretrained long document summarizer:
  * [Arman Cohen's point-generator network (tf)](https://github.com/armancohan/long-summarization)
  * [Wendy Xiao's global+local extractive summarizer](https://github.com/Wendy-Xiao/Extsumm_local_global_context)
  * [Andrei Mircea's BERT-based extractive summarizer](https://github.com/mirandrom/HipoRank)
* run summarizer on the NLPIA book, 3 parts, 14 chapters, sections, paragraphs
* tree structure with headings, like TOC by expandable

### Other Considerations

Build a webapp in FastAPI/Django to deploy one of the projects using DevOps and MLOps concepts?

## Notes from Week 2 Meeting with Hobson

Decided on a Project to work on: **Book summary**

While a whole book summary might be far-fetched; we'll start with summary of paragraphs then scale outwards.

Based on initial discussions, project would invlove:

* NLU and Extraction Analysis.
* OR Abstractive summarization - Generative model that generates a new sentence

### Planning

Week 1

* Look up previous works on Natural Language Summarization

  In that regard, I found:

  * [HuggingFace Summarisation](https://huggingface.co/tasks/summarization)
  * [bart-large-cnn](https://huggingface.co/facebook/bart-large-cnn)
  * [Text Summarization with Pretrained Encoders](https://github.com/nlpyang/PreSumm)

* Determine datasets to use

  I found:
  
  * [CNN-DailyMail](https://huggingface.co/datasets/cnn_dailymail#data-instances)
  * [WIkiHow](https://github.com/mahnazkoupaee/WikiHow-Dataset)

Weeks 2 & 3

* Select an already established work to test and implement
* Initial Machine Learning on the dataset

Week 4

* Build?

### Tasks

* Learn What other people have done on natural language summarization. 

### Miscellaneous

* Rouge score - scoring ML model summary against test set by checking the words
* Lookup Semantic search - intent analysis
