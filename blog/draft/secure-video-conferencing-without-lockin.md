# Video conferencing

Finding an open source application or webapp for videoconferencing is a challenge.
It's even harder to find someone who can host that software for your reliably and securely.

## Jitsi Meet hosted on 8x8

attendees: https://meet.jit.si/moderated/40cb222dac46231c5674f09d7ab8c55663d4f31b45fd0f5b0a9bcb44fece0ef1

moderators: https://moderated.jitsi.net/2d105c06cc4849e4b88a8bc6b1c8d1e540b00a76ec8647859c93221a7bcbc1a7

## Big Blue Button hosted on elos.vc

Like the short customized URL, even for free accounts. But make sure you have a table internet connection (probably without a VPN) before you connect to a BBB server.

https://elos.vc/hobs/

## Self-hosted Jitsi Meet:

$40-$50/mo on Digital Ocean or render.com
