# FOSS community best practices

Had a conversation with Leo about how to build and grow a FOSS community.

## Examples

- Steven Skoczen's Will (chatbot) [community](http://skoczen.github.io/will/) is a good example of a project that lasted nearly 4 years with active contributors and no funding.
- Zulip (FOSS chat/forum app similar to Slack) [community](https://zulip.com/developer-community/) runs awesome hackathons at PyCon.
- Sci-Kit Learn has a divers group of expert contributors
- NumPy
- VS-Code grew rapidly with it's plugin support
- Sublime Text is closed source but makes it easy to add plugins and hundreds of contributors have over the years 


## Growing open source communities usually...

- Led by a single open-minded person with a vision and time to support her contributors
- Use TDD
- Use CICD
- Use chatops and other automation
- Have a public-facing webapp associated with the project
- Have contributors.md file with instructions/templates triggered by Merge Requests
- Have an authors.md file that contributors are encouraged to update with their details
- Don't have a Contributor License Agreement [cla](https://ben.balter.com/2018/01/02/why-you-probably-shouldnt-add-a-cla-to-your-open-source-project/)
- Have a slack or discord for decisionmaking or socializing
- Have sponsors to support servers and resources, but not to pay contributors
- Have a jobs board
- Have a list of organizations where the software is used
- Have a "powered buy" badge or button
- Have a logo and domain name
- Connect collaborators to each to other
- Have good conversation facilitators or "developer support" engineers on staff
- Have good install and usage instructions in a README.md
- Have a good faq
- Track and respond to issues, bug reports, feature requests
- Have a bug report template that includes "Reproduce" and "Desired behavior" sections
- Active contributor passionate about security to avoid [this](https://www.cisecurity.org/advisory/a-vulnerability-in-an-npm-package-could-allow-for-remote-code-execution_2021-136/)

## What about competitors?

- open source projects innovate faster and add features faster than close projects
- a competitor can almost always reverse-engineer any code you keep private, and often do it better because they have the advantage of seeing what works and doesn't work with your product
- because your team is familiar with the stack and technology, and you created the initial project, you will have no difficulty convincing customers that they should trust you over other companies that just "use" your FOSS software rather than actively maintaining it
- you will be able to steer the community towards features your customers need over those that other contributors' customers need

## What about security?

- open source projects are historically much more secure than closed source projects
- Linux desktops are rarely hacked, but Windows machines (servers and desktops) are at fault in nearly all recent security breaches

## What about sabotage?

- only sociopaths try to sabotage an open source project
- sociopaths try to hide their anti-social tendencies by ensuring they can always deny that they were to blame for any problem
