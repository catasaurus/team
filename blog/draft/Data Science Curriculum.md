# Data Science Curriculum

## TODO

- asciify this markdown
- blog post monthly on each bullet point
- edit blog posts to self-publish textbook
- find the rikeem curriculum and merge with this

## TOC

- metacognition
  - active learning
  - reproduce others' work
- anaconda & git-bash & git
- ipython and text editor (sublime or vscodium)
- What is ML, DS, AI
- 4 problems
- 8 kinds of data
  - binary (bool): 0, 1 (true/false)
  - ordinal (int): 18, 21, 65
  - real (continuous numerical, float):
  - NaNs / blanks / missing-values
  - nonbinary categorical: gender, color, roof type
  - time (including date)
  - natural language
- dealing with missing values
- dealing with categoricals
  - drop 1?
  - NaNs? Blanks? (just another category)
- dealing with nans in continuous variables
- linear regression
- transforming numerical values
  - scaling
  - 
- multivariate linear regression
- home prices (zestimate)
- DOTA 2 hero portfolio optimization
- DnD party optimization
- representations of data
- feature engineering
    - visualizations (thresholds, transformations, dimension reduction)  
- dimension reduction (PCA, CCA, Discriminant Analysis, TSNE)
- NLP
- sparse arrays (TFIDF)
- time series (ARMA ARIMA prophet)
- RNN, LSTM, Transformer (stock prices, translation, reading comprehension, summarization)
- active learning
- neural nets
- memory mapped datasets (hdf5)
- lightgbm lgbm

- Hyperparameter Tuning
  - must rigorously record your experiments so they are reproducible
  - Kfold? no!
  - manual guess, single parameter
  - manual guess, multiple parameter (what most people do
  - array (for loop)
  - 
  - Bayesean


## Hyperparameter tuning (model optimization)

  - must rigorously record your experiments so they are reproducible
  - CV? Kfold? no! 2-fold? maybe but usually just want a small val set and large train set
  - manual guess, single parameter
  - manual guess, multiple parameter (what most people do
  - array (for loop)
  - Bayesean
  - Hyperbayesean AI (sped up by prioritizing easier/simpler models and using that info to inform choices on more complicated models)
    - optimize 2 layers, then 3 layers, then extrapolate to 4 layers etc. generalize/regularize your bayesean hyperparam model
    - algorithms then split layers that into feature engineering or preprocessing steps (stopword filtering, scaling, keyword extractors, data augmentation transforms, robustification approaches)

#### Diagram of Model Tuning on top of (around?) ML

### What is a parameter/parameterization

### Model parameters vs model hyperparameters

### Why do you care about hyperparameters?

#### Model parameters affect
- speed: the algorithms and thus the resources & speed (RAM, disk, io, CPU, GPU cores)
- generalization: of the model (avoid over fitting)
- "capacity": (complexity) of the model (avoid under fitting)

Speed, capacity (brain power), generalization, and speed are the essence of intelligence.
When you give your model a test with the test set, you care not only on the score or number of questions it gets right, but you care how fast it does it and how well it will work on real world questions IRL in the wild.
So you're helping your ML model become a smarter model of the world.

#### Resources
- the speed at which the model trains/learns (efficiency per flop)
- the memory the model consumes (efficiency in RAM)
- the IO the 
- the parallelizablity of the 
- affect the efficiency of learning (per example)

### How do you tell what is optimal

- measures of accuracy
- robustifying your test set
- active learning?
- measures of outcomes (stakeholders care about)
- how much resources to run ($)
- how much resources to maintain ($)
- how explainable or intelligible or simple/elegant the model is (confidence in causality)

### No right answer

You will develop an intuition for guessing good hyperparameters:

- multiple local optima that may be equivalent or have tradeoffs for different applications
- different datasets will have different hyperparams
- different problems (real world dynamical systems) 

### Approaches

- grid

### meta-meta parameters (model architecture)

Most people think of this as nested search, creating a family of models that are each tuned separatedly.
Better to integrate all the hyperparameters into one decision making algorithm (bayes optimizer).


- just another hyperparameter
- should be randomly sampled searched just as your other parameters are




### References

• “Random Search for Hyper-Parameter Optimization.” James
Bergstra and Yoshua Bengio. Journal of Machine Learning
Research, 2012.
• “Algorithms for Hyper-Parameter Optimization.” James Berg‐
stra, Rémi Bardenet, Yoshua Bengio, and Balázs Kégl.” Neural
Information Processing Systems, 2011. See also a SciPy 2013 talk
by the authors.
• “Practical Bayesian Optimization of Machine Learning Algo‐
rithms.” Jasper Snoek, Hugo Larochelle, and Ryan P. Adams.
Neural Information Processing Systems, 2012.
• “Sequential Model-Based Optimization for General Algorithm
Configuration.” Frank Hutter, Holger H. Hoos, and Kevin
Leyton-Brown. Learning and Intelligent Optimization, 2011.
• “Lazy Paired Hyper-Parameter Tuning.” Alice Zheng and
Mikhail Bilenko. International Joint Conference on Artificial
Intelligence, 2013.
• Introduction to Derivative-Free Optimization (MPS-SIAM Series
on Optimization). Andrew R. Conn, Katya Scheinberg, and Luis
N. Vincente, 2009.
• Gradient-Based Hyperparameter Optimization Through
Reversible Learning. Dougal Maclaurin, David Duvenaud, and
Ryan P. Adams. ArXiv, 2015
* jumping over the Pitfall!(s) of A/B testing
