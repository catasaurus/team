# Google N-Gram Viewer

# 1-Grams

```python
>> > url = 'http://storage.googleapis.com/books/ngrams/books/20200217/eng/totalcounts-1'
>> > df = pd.read_csv(url, lineterminator='\t', header=None)
>> > df.columns = 'year match_count page_count volume_count'.split()
>> > df.head()
    year  match_count  page_count  volume_count
0  1470          984          10             1
1  1472       117652         902             2
2  1475       328918        1162             1
3  1476        20502         186             2
4  1477       376341        2479             2
```

Wow, they found a book that was published in 1470!
And two books from 1472!
That's twenty years before the Spanish started colonizing North America.

So, what have you done lately, Mr. N-gram Viewer?

```python
>>> df.tail()
    year  match_count  page_count  volume_count
524  2015  26752986914   132432798        294787
525  2016  26697166061   129083200        281826
526  2017  27495000308   136650069        281558
527  2018  26305614574   137064469        261781
528  2019  22826152232   121332926        258273
```

Wow, those are big numbers. You can make them a little more human - readable if you divide by 1 million:

```python
>>> df.tail() / 1000000
    year   match_count  page_count  volume_count
524  0.002015  26752.986914  132.432798      0.294787
525  0.002016  26697.166061  129.083200      0.281826
526  0.002017  27495.000308  136.650069      0.281558
527  0.002018  26305.614574  137.064469      0.261781
528  0.002019  22826.152232  121.332926      0.258273
``

So in recent years Google has been indexing almost 300k new English language books per year!
And the number of published books or volumes may be on the decline.
Or maybe there's a lag between books being published and Google finding them and scanning them.
Interesting.

And they've tokenized about 25B words per year.
