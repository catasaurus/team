""" Tutorial on recurrence and memoization factorials and e

## References

### Wikipedia Article Series on *e*

[A series of articles](https://en.wikipedia.org/wiki/Category:E_(mathematical_constant)
on the mathematical constant _*e*_

* [Natural logarithm](https://en.wikipedia.org/wiki/Natural_logarithm "Natural logarithm")
* [Exponential function](https://en.wikipedia.org/wiki/Exponential_function "Exponential function")
* [compound interest](https://en.wikipedia.org/wiki/Compound_interest "Compound interest")
* [Euler's identity](https://en.wikipedia.org/wiki/Euler%27s_identity "Euler's identity")
* [Euler's formula](https://en.wikipedia.org/wiki/Euler%27s_formula "Euler's formula")
* [half-lives](https://en.wikipedia.org/wiki/Half-life "Half-life")
* exponential [growth](https://en.wikipedia.org/wiki/Exponential_growth "Exponential growth")
"""


def factorial_dumb(x):
    """ Compute x! = x * (x - 1) * (x - 2) ... 1  #  x! = 1 for x == 1 or x == 0

    >>> factorial(0)
    1
    >>> factorial(1)
    1
    >>> factorial(3)
    6
    """
    if x in {1, 0}:
        return 1
    else:
        return x * factorial(x - 1)


def factorial(x, memo={}):
    """ Compute x! = x * (x - 1) * (x - 2) ... 1  #  x! = 1 for x == 1 or x == 0

    >>> factorial(0)
    1
    >>> factorial(1)
    1
    >>> factorial(3)
    6
    """
    if x in {1, 0}:
        return 1
    elif x in memo:
        return memo[x]
    retval = x * factorial(x - 1, memo)
    memo[x] = retval
    return retval
