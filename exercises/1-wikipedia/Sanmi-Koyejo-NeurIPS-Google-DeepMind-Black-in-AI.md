# Sanmi-Koyejo

Wikipedia article lacks sufficient references according to the banner warning on the DRAFT article page.
Below is some recent media coverage that should help.

## Selected Recent News

[Sanmi Koyejo](https://sanmi.cs.illinois.edu/)

## Selected Recent News

-   I am serving as a General co-chair for NeurIPS 2022 with [Shakir Mohamed](https://shakirm.com/).
-   Some [media coverage](https://www.auntminnie.com/index.aspx?sec=ser&sub=def&pag=dis&ItemID=134420) of our recent work on detecting health disparities in COVID-19 patients.
-   Our group won an [NSF CAREER award](https://cs.illinois.edu/news/koyejos-nsf-career-award-to-shape-probabilistic-models-for-neuroscience).
-   I am honored to win the [2021 Skip Ellis Early Career Award](https://cra.org/cra-wp/scholarships-and-awards/awards/skip-ellis-early-career-award/) from the CRA-WP. Clarence “Skip” Ellis was the first African-American to earn a PhD in computer science ([link to a brief writeup about Ellis](https://cs.illinois.edu/about/awards/alumni-awards/alumni-awards-past-recipients/clarence-ellis)).
-   Congrats to Wale Saludeen, who was selected as a [Miniature Brain Machinery trainee](https://minibrain.beckman.illinois.edu/trainees/).
-   Congrats to Wale Saludeen, who was selected to receive a 2021 Illinois GEM Associate Fellowship.
-   Congrats to Zachary Robertson, who received a [Wing Kai Cheng fellowship](https://cs.illinois.edu/about/awards/graduate-fellowships-awards/wing-kai-cheng-fellowship).
-   Congrats to Brando Miranda, who received an honorary mention for the [Ford Fellowship](https://nrc58.nas.edu/FordFellows20/ExtRpts/PressReleaseRoster.aspx?RptMode=HM&CompYr=2021).
-   Our group won a [Alfred P. Sloan fellowship in computer science](https://cs.illinois.edu/news/Koyejo-wins-2021-Sloan-Research-Fellowship).
-   I am serving as a workshop chair for NeurIPS 2021.
-   [Some media](https://cs.illinois.edu/news/shot-in-the-dark-provides-path-toward-collaborative-research-that-better-predicts-covid-19-severity) covering our recent work on co-morbidity prediction from Xrays.
-   Our group is participating in the NIH funded [MIDRC](https://www.midrc.org/), aimed at accelerating the transfer of knowledge and innovation in the current COVID-19 pandemic. See the team [here](https://www.midrc.org/midrc-team).
-   Our group is a member of the new NSF and NIFA AI research institute on Artificial Intelligence for Future Agricultural Resilience, Management and Sustainability [(AIFARMS)](https://digitalag.illinois.edu/research/aifarms/), managed by the center for digital agriculture [(CDA)](https://digitalag.illinois.edu/), ([media](https://cs.illinois.edu/news/illinois-leads-new-national-ai-institutes-adve-directs-aifarms)).
-   In collaboration with OSF healthcare, our group was awarded a C3.ai Digital Transformation Institute grant to develop “Secure Federated Learning for Clinical Informatics with Applications to the COVID-19 Pandemic” ([announcement](https://c3.ai/c3-ai-digital-transformation-institute-announces-covid-19-awards/)) ([news](https://cs.illinois.edu/news/c3ai-dti-awards-5.4M-AI-Research-COVID-19-funds-three-illinois-cs-projects)). Collaborators: [Khurana](https://www.dakshitakhurana.com/), [Heintz](https://healtheng.illinois.edu/people/joerg-george-heintz/), [Bond](https://www.jumpsimulation.org/biography-pages/jump-staff/william-bond) and Foulger.
-   In collaboration with OSF healthcare, our group was awarded Jump Arches COVID-19 grant to develop “Secure Federated Learning for Collaborative Diagnostics” [(news)](https://newsroom.osfhealthcare.org/spring-2020-jump-arches-grants-focus-on-rapid-solutions--for-covid-19-response/). Collaborators: [Khurana](https://www.dakshitakhurana.com/), [Heintz](https://healtheng.illinois.edu/people/joerg-george-heintz/), [Bond](https://www.jumpsimulation.org/biography-pages/jump-staff/william-bond) and Foulger.
-   Our group was co-awarded an NSF HDR TRIPODS grant for the “Illinois institute for data science and dynamical systems (IDS2)” [(website)](https://publish.illinois.edu/ids2-tripods/). See our excellent collaborators [here](https://publish.illinois.edu/ids2-tripods/people/).
-   Congrats to Brando Miranda, who was just named a Hispanic Scholarship Fund (HSF) scholar and received an honorary mention for the Ford Fellowship.
-   Congrats Wale Saludeen, who was selected to receive a 2020 Beckman Institute Graduate Fellowship [(news)](https://beckman.illinois.edu/about/news/article/2020/05/08/seven-named-2020-beckman-institute-graduate-fellows)
-   Congrats to Katherine Tsai, who was selected to receive a 2020 NSF Fellowship.
-   I am serving as diversity and inclusion co-chair for [AISTATS 2021](http://aistats.org/aistats2021/).
-   I am serving as a workshop co-chair for [ICLR 2021](https://iclr.cc/Conferences/2021).
-   I am serving as a workshop co-chair for [NeurIPS 2020](https://nips.cc/Conferences/2020/Committees).
-   [Slides(pdf)](https://sanmi.cs.illinois.edu/documents/Representation_Learning_Fairness_NeurIPS19_Tutorial.pdf) from the NeurIPS 2019 tutorial on Representation Learning and Fairness (with Moustapha Cisse) are now availiable [\[video (external link)\]](https://slideslive.com/38921491/representation-learning-and-fairness)!
