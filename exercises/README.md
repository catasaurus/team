# Tangible AI Internship Syllabus

## Exercises for Interns and Lifelong Learners

The subdirectories here each contain an exercise that would take a professional developer an hour or two.
As you are learning, if you have mastered the prerequisite skills, the exercise could take a week (10 hours or more of focussed effort).
If it takes longer than that, recalibrate by going back to earlier exercises to make sure to have built the "scaffolding" in your brain to support the new learning.

## Typical Internship Syllabus

1. skills checklist and self-assessment using gitlab (fork, create new file, copy/paste/edit)
2. plan a project: http://proai.org/wk1
3. git from the command line
4. prosocial AI research, regenerative business, prosocial networking
5. command line (bash) manipulation of files and directories
6. Fake news classification
7. project proposal and plan
8. skills checklist review
9. draft final report
10. blog post & linked-in announcement of your project/portfolio
11. edit or add an exercise in the team repo: http://proai.org/wk10

## TODO: Exercises that need polishing

- [ ] Add links to list above
- [ ] add more exercises to the syllabus above
- [ ] flesh out some of the ideas below


## TODO: Exercise ideas

### Active Learning

Active learning exercises categorized as data, numpy, dataframe, series, strings, django, flask, builtin.
Have many "doctests" that produce error messages.
Have many math/order of operations examples that are tricky, similar to exercises for Springboard.


- qary skill that displays a line of code and has you type the doc test result
- list('Abba')
- set('Abba')
- np.array('Abba')
- pd.DataFrame()

### Computational thinking
computational thinking exercises to explain:

- REPL active learning approach (always examine variables until your mental model is perfect)
- variable names
- string literals vs variable names, built-ins, name spaces, object attributes, str manipulation (str.strip, str.lstrip, ''.strip(' '), .replace(' ', ''), etc.)


# TangibleAI Resource Repository

## TBD: Table of Contents
1. [Command Line](#command-line)
2. [Conda](#conda)
3. [DigitalOcean](#digitalocean)
4. [Dyshel](#dyshel)
5. [Git](#git)
6. [GitLab](#gitlab)
7. [Hobson](#hobson)
8. [Markdown](#markdown)
9. [TangibleAI](#tangibleAI)
10. [Qary](#qary)

### [Command Line](./commandline/README.md)
[Table of Contents](./commandline/readme.md#table-of-contents)
### [Conda](./conda/README.md)
[Table of Contents](./conda/README.md#table-of-contents)
### [DigitalOcean](./digitalocean/README.md)
[Table of Contents](./digitalocean/README.md#table-of-contents)
### [Dyshel](/dyshel/README.md)
[Table of Contents](./dyshel/README.md#table-of-contents)
### [Git](./git/README.md)
[Table of Contents](./git/README.md#table-of-contents)
### [GitLab](./gitlab/README.md)
[Table of Contents](./gitlab/README.md#table-of-contents)
### [Hobson](./hobson/README.md)
[Table of Contents](./hobson/readme.md#table-of-contents)
### [Markdown](./markdown/README.md)
[Table of Contents](./markdown/readme.md#table-of-contents)
### [TangibleAI](./tangibleai/README.md)
[Table of Contents](./tangibleai/readme.md#table-of-contents)
### [Qary](./qary/README.md)
[Table of Contents](./qary/readme.md#table-of-contents)


### [resources.yml](./resources.yml)

Example yml video schema:

```
-
    title: name of the Video or Document
    url: associated digitalocean url
    duration: approximate duration of video
    authors: contributing authors of the work
    tags:
        - first tag denotes the resource folder (or primary topic) of the video
        - second tag denotes the digital ocean source folder
        - additional tags to denote keywords.
-
```

## 


