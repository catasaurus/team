Take home test ideas.

Work on one of the following mini projects and report back on what you learn:

### 1. Django (Web development)
- Do the Django tutorial (your first Django app)
- Describe a data model you would use for an app that allows you to log the amount of time you spend on various projects


### 2. Web APIs (Networking & basic NLP): 
- Do some python `requests` tutorial or skim the documentation
- Exercise an NLP API like google translate, Google Comprehend, Amazon lex, or GPT-3
- What do you think? What kind of application might you be able to build using that NLP service? 


### 3. SpaCy API (DevOps, Web Dev & NLP):
- Install Docker on your laptop or create a VM on Digital Ocean or AWS with Docker installed
- Think of a useful NLP (NLG or NLU service) that you would like to deploy as an API (see Amazon and Google APIs for examples)
- Deploy a Docker container with an NLP endpoint, for example [SpaCy](https://github.com/jgontrum/spacy-api-docker)


### 4. (ML, NLP, DevOps)
- Play around with heartex's Label Studio for 14 days in their free trial
- Label some NLP data within Label Studio
- Install the open source version of Label Studio on your desktop or a cloud server


### 5. Robust NLP (Advanced NLP)
- a. Research the concept of robust NLP and explain it to us
- b. Play with the http://dynabench.org question answering model and see if you can fool it.
- c. Play with the dynabench NLI model and see if you can fool it. 
- d. Play with the http://dynabench.org hate-speech detector and try to fool it.
- e. Describe to us what you learned about these models and about Robust NLP


### 6. NLP for Education (Advanced NLP) 
- Play around with Vish's cleverly.io app. 
- What ideas do you have for improving it? 
- How you would implement each feature? Web framework? Database? Data model? ML models? Cloud services?
- What do you think about the Learning Engineering possiblities for the app?


### 7. Tanbot (NLP & Devops) 
- Fork the [tanbot repository](https://gitlab.com/tangibleai/tanbot). 
- Create a VM on digital ocean (we will pay you $50 for the cost of a small Droplet)
- Configure the your forks GitLab repo environment variables so that the CI-CD pipeline deploys it to your VM


### 8. FastAPI for `qary` (Web Dev & NLP) 
- Create a medium Droplet (VM) on DigitalOcean or AWS (we will pay you $50)
- Create a [fastapi app](https://fastapi.tiangolo.com/) ("Hello world" endpoint is fine)
- Deploy your [FastAPI app to Digital Ocean](https://pythonawesome.com/deploy-a-fastapi-application-into-production-digitalocean-app-platform/)
- Add an endpoint for the [qary chatbot](https://gitlab.com/tangibleai/tanbot) skill "Eliza"


### 9. Intent Classification & Clustering (Advanced NLP, independence & creativity)
- Create a jupyter notebook environment with the NLP tools you will need for "intent classification" NLU
- Find a set of utterances from any chatbot labeled with their intent or label your own from twitter or Slack or E-mail us for a CSV with 100s of statements from users of a chatbot.
- Train and evaluate an intent classifier on your dataset of utterances
- Create an unsupervised intent clusterer (grouper) that can handle unlabeled utterances
- Make sure that a large number of your utterances are unlabeled (or remove all of the labels for at least one category of intents in your dataset.
- Goal: your model correctly labels a test set formed from the labeled examples and classify the unlabled ones as "unlabled"
- Stretch goal: your model correctly labels some of the unlabeled utterances that belong within one of the valid intent labels from the original dataset.

