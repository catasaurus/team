# Envision@Princeton

## Adam

(haven't looked at your slides yet)

* Introduction
* Ed Tech
* EdCheck

## Hobson

1 slide each

* Tech is not the problem humans are
* AI alignment (beneficial AI)
* Objective functions & incentives (for bots and humans)
* Post-scarcity era
* Post-truth era
* Engineers are the last line of defense against dystopia
* 4+1 interactive case studies ("Karma Clues", "Moral Machines", "AI Ethics")
1. Starbucks: Barista incentives
2. Tesla: Self driving car (moralmachine.net)
3. Google: Search engine
4. Aira: Visual interpreter (people with low-vision)
5. EdCheck: math tutor bot

## My Latest Favorite Tools

As an engineer these are the tools I trust right now, because they are from "Aligned Businesses"

1. [Wikipedia](https://www.wikipedia.org)
3. [GitLab](https://gitlab.com)
2. [Papers With Code](https://paperswithcode.com)
4. [Telegram](https://www.telegram.org)
5. [Mastodon](https://mastodon.social)
6. [EdEx](https://edex.org)
7. [Free Code Camp](https://www.freecodecamp.org)
8. [Simplicable](https://simplicable.com)
9. [Tutanota](https://tutanota.com)
10. [Next Cloud](https://nextcloud.org)
11. [NN Playground](https://github.com/destenson/tensorflow--playground)

## References

* Freakonomics (Steve Leavitt and Steven Dubner)
* Consequences of Capitalism (Noah Chomsky)
* Human Compatible (Stuart Russell)
* 21 Lessons of the 21st Century (Yuval Harari)
* The Sparrow, 3-Body Problem, Blindsight (Russell, Liu, and Watts) 
