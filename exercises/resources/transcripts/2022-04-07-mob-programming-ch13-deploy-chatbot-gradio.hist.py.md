# Mob Programming - Deploy a chatbot with gradio

April 6, 2022

Testing the qary chatbot dialog engine (v3).

## Links

- https://gitlab.com/tangibleai/experiments/-/tree/main/gradio
- https://gitlab.com/tangibleai/qary/-/blob/main/src/qary/data/chat/moia-poly-dialog-tree-simplified-chinese.v3.dialog.yml

```python
from qary.chat.v3 import *
e = Engine()
e.states
e.states['select-language']
e.run('English')
e.state_name
e.states
e = Engine()
e.state_name
e.run('ch')
e.run('zh')
e = Engine()
e.run('zh')
e.state_name
e.states[e.state_name]
```
