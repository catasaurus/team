# Python Programming

topics:
-
  topic: getting help
  subtopics: 
    - "`help()`"
    - "`?`"
    - "`??`"
    - "`[TAB] completion`"
    - "`vars()`"
    - "`dir()`"
    - "`type()`"
    - "`print()`"
    - "`.__doc__`"
  questions:
    -
      q: "`print(1, 2)`"
      a: 1 2
    -
      q: "`print('1, 2')`"
      a: 1, 2
-
  topic: importing python packages
  subtopics: ["`this`", "`os`", "`sys`", "`sys.path_append()`"]
-
  topic: finding, installing, importing python packages
  subtopics: ["`sklearn`", "`Pandas`", "`tqdm`"]
-
  topic: reading Tracebacks and error messages
  subtopics: ["`KeyError`", "`TypeError`", "`ValueError`", insidious errors]
-
  topic: conditional expressions
  subtopics: ["`if`", "`else`", "`>`", "`<`", "`==`", "`!=`", "`not`", "`in`" ]
-
  topic: loops
  subtopics: ["`for`", "`while`", "`enumerate`", "`zip`", "`zip*`", "`tqdm`"]
-
  topic: functions
  subtopics: [args, kwargs, "`def`", "`return`"]
-
  topic: object oriented programming
  subtopics: ["`class`", attributes, methods, args, kwargs, "`__init__`", "everything is an object"]
-
  topic: scalar numerical data types
  subtopics: ["`int`", "`float`", "`complex`"]
-
  topic: sequence data types
  subtopics: ["`str`", "`bytes`", "`list`", "`tuple`"]
-
  topic: mapping
  subtopics: ["`dict`", "`Counter`"]
-
  topic: type coercion
  subtopics: ["`list(str)`", "`dict([(1,2)]`", "`dict(zip(range(4), 'abcd'))`"]
-
  topic: sets, categories, tags, dummy variables, bag of words
  subtopics: ["`set`", "`Counter`"]
-
  topic: files
  subtopics: ["`with`", "`open`", "`write`" ]
-
  topic: manipulate files with bash
  subtopics: ["`pwd`", "`!`", "`mkdir ~/code`", "`mv`", "`cp`", "`touch`"]
-
  topic: navigate directories
  subtopics: ["`cd`", "`pwd`", "`ls -hal`", "`/`", "`~`", "`*`", "`$HOME]`"]

# These are called doctests. The question is the python code the line prefixed with `>>>`. The correct answer is the value you would put on the line after the python code based on what that expression would output.


# ```python
# >>> dict(enumerate('Hello'))[1]
# 'e'
# ```

# ```python
# >>> dict(enumerate('Hello'))[:-1]
# TypeError: unhashable type: 'slice'
# ```

# ```python
# >>> set('Oh Hello'.lower()) - set('Ohio ')
# {'e', 'l'}
# ```

# ```python
# >>> dict(Counter(list('ABBA'))
# Counter({'A': 2, 'B': 2})
# ```

# ```python
# >>> dict(zip(*['RD', '2'*2]))
# {'R': '2', 'D': '2'}
# ```

# ```python
# >>> pwd
# >>> set(Path('.').absolute().__str__()) - set(_)
# set()
# ```

# ```python
# >>> def f(x):
# ...     while x > 2:
# ...         x -= 2
# ...     return x
# >>> f(128)
# 2
# ```

# ```python
# >>> 'lower' in dir('ect')
# True
# ```

# ```python
# >>> 'dir' in list('direct')
# False
# ```

# ```python
# >>> [1, 2, 3] * 2
# [1, 2, 3, 1, 2, 3]
# ```

# ```python
# >>> np.array([1, 2, 3]) * 2
# [2, 4, 6]
# ```

# ```python
# >>> pd.Series([1, 2, 3]) - 1
# 0    0
# 1    1
# 2    2
# dtype: int64
# ```

# ```python
# >>> pd.Series([1, 2, 3]) + pd.Series([1, 2, 3], index=[1, 2, 3])
# 0    NaN
# 1    3.0
# 2    5.0
# 3    NaN
# ```
